Pymato means PYthon and MAstodon plays TOgether.

At the moment, this script provides you the feature to delete all your toots from a mastodon account without destroying it.
As the api is limited in its actions, we have to remove the toots per 20.

You have the possibility to remove all your toots from a thread by providing the ID of one of your toot inclunding in the thread.

Nevertheless, the links between the differents toots of the thread will be destroyed.

First, you have to install the python wrapper for Mastodon using the following command line:

pip install Mastodon.py

You can have more informations about this library here:

https://github.com/halcy/Mastodon.py

At the first use, you have to modify the script to provide a login and a password and to authorize the applications to talk with your mastodon instance.

This can be done here:

https://{myinstancebaseurl}/oauth/authorized_applications

example for the instance relative to "La quadrature du net":

https://mamot.fr/oauth/authorized_applications

if you need to contact me for any reasons

https://mamot.fr/@darkhelmeut

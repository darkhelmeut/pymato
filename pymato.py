#!/usr/bin/python

try:
       from mastodon import Mastodon
except ImportError:
       print("You have to install the library Mastodon to use this script. Please type the following command 'pip install Mastodon.py'")

import yaml
from pathlib import Path
import argparse
import time 
import sys
import os
DIR=os.path.dirname(os.path.realpath(__file__))

with open(DIR+"/credentials.yaml","r") as stream:
    try:
        config = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print (exc)


# provide these datas in the file credentials.yaml
CONSTANT_USER_LOGIN=config["login"]
CONSTANT_USER_PASSWORD=config["password"]
CONSTANT_API_BASE_URL=config["instance"]


CONSTANT_APP_SECRET="app.secret"
CONSTANT_USER_SECRET="user.secret"

app_secret_file = Path(DIR+"/"+CONSTANT_APP_SECRET)

if not app_secret_file.exists():
    print("get the app secret file")
    Mastodon.create_app(
     'darkyapp',
      api_base_url = CONSTANT_API_BASE_URL,
      to_file = DIR+"/"+CONSTANT_APP_SECRET
    )

user_secret_file = Path(DIR+"/"+CONSTANT_USER_SECRET)

if not user_secret_file.exists():
  print("get the user secret file")

  mastodon = Mastodon(
    client_id = DIR+"/"+CONSTANT_APP_SECRET,
    api_base_url = CONSTANT_API_BASE_URL
  )

  mastodon.log_in(
    CONSTANT_USER_LOGIN,
    CONSTANT_USER_PASSWORD,
    to_file = DIR+"/"+CONSTANT_USER_SECRET
  )

mastodon = Mastodon(
  client_id = DIR+"/"+CONSTANT_APP_SECRET,
  access_token = DIR+"/"+CONSTANT_USER_SECRET,
  api_base_url = CONSTANT_API_BASE_URL
)

userinfos = mastodon.account_verify_credentials()
userid = userinfos.get('id')


def delete():
    index = 1

    try:
        statuses = mastodon.account_statuses(userid)

        if( len(statuses) > 0 ):

            for status in  mastodon.account_statuses(userid):
                print(str(status.get('id') ) + " was deleted")
                time.sleep(1)
                mastodon.status_delete( status.get('id') )
                

            delete() 
        else:
            exit()
    except ValueError as e:
        print (e)


def thread(option):

    try:
        main_id = option
        # we get the context
        context = mastodon.status_context( main_id )

        if bool( context.get('ancestors') ):

            thread( context.get('ancestors')[0].get('id') ) 
       
        else: 
    
            for status in context.get('descendants'):
                if status.get("account").get("id") == userid:
                    time.sleep( 1 ) 
                    print ( str(status.get('id') ) + " was deleted" )
                    mastodon.status_delete( status.get('id') )

            mastodon.status_delete( main_id )

    except ValueError as e:
        print (e)

def rmfav():
    index = 1

    try:
        statuses = mastodon.favourites()

        if( len(statuses) > 0 ):

            for status in  statuses:
                print(str(status.get('id') ) + " was unfavorite")
                time.sleep(1)
                mastodon.status_unfavourite( status.get('id') )
                

            rmfav() 
        else:
            exit()      
    except ValueError as e:
        print (e)


parser = argparse.ArgumentParser()


parser.add_argument('-t','--thread', action='store', dest='thread', nargs=1, type=int, help="delete all your toots");
parser.add_argument('-d','--delete', action='store_true', dest='delete',  help="delete all your toots");
parser.add_argument('-f','--rmfav',  action='store_true', dest='rmfav',   help="delete all your favorites");

args = parser.parse_args()

if(args.delete):
    delete()
if(args.rmfav):
    rmfav()
if(args.thread):
    thread(args.thread[0])

